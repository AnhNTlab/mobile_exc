import 'package:get_it/get_it.dart';
import 'package:mobile_exercise/global/app_navigation.dart';

import 'config/config.dart';

void setupLocator(){
  GetIt.I.registerLazySingleton(() => Navigation());

  GetIt.I.registerSingleton<AppConfig>(
    AppConfig(),
  );
}