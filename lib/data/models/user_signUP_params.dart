class UserSignUpParams {
  String emailOrPhoneNumber;
  String password;
  String role;
  String userName;
  String name;

  UserSignUpParams(
      {this.emailOrPhoneNumber, this.password, this.role, this.userName,this.name});

  UserSignUpParams.fromJson(Map<String, dynamic> json) {
    emailOrPhoneNumber = json['emailOrPhoneNumber'];
    password = json['password'];
    role = json['role'];
    userName = json['userName'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['emailOrPhoneNumber'] = this.emailOrPhoneNumber;
    data['password'] = this.password;
    data['role'] = this.role;
    data['userName'] = this.userName;
    data['name'] = this.name;
    return data;
  }
}