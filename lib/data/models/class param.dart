class ClassCreateParams {
  String description;
  String name;

  ClassCreateParams({this.description, this.name});

  ClassCreateParams.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this.description;
    data['name'] = this.name;
    return data;
  }
}