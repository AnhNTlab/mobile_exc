import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:mobile_exercise/data/models/class%20param.dart';
import 'package:mobile_exercise/data/models/user_login_params.dart';
import 'package:mobile_exercise/data/models/user_signUP_params.dart';
import 'package:mobile_exercise/domain/models/test.dart';
import 'package:mobile_exercise/domain/models/user.dart';
import 'package:mobile_exercise/domain/repository/account_repository.dart';
import 'package:mobile_exercise/instance/Session.dart';

class AccountRepositoryIml extends AccountRepository {
  Dio _dio = Dio();

  @override
  Future<void> signUP(UserSignUpParams userSignUpParams) async {
    try {
      (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient client) {
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      };
      String path =
          "http://35.240.198.195:8080/api/author/v1/register?emailOrPhoneNumber={emailOrPhoneNumber}&name={name}&role={role}&userName={userName}&password={password}"
              .replaceAll("{emailOrPhoneNumber}",
                  userSignUpParams?.emailOrPhoneNumber ?? "")
              .replaceAll("{name}", userSignUpParams?.name ?? "")
              .replaceAll("{role}", userSignUpParams?.role ?? "")
              .replaceAll("{userName}", userSignUpParams?.userName ?? "")
              .replaceAll("{password}", userSignUpParams?.password ?? "");

      await _dio.post(path);
    } catch (e) {
      throw e;
    }
  }

  @override
  Future<void> login(UserLoginParams userLoginParams) async {
    try {
      (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient client) {
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      };

      var response = await _dio.post(
          'http://35.240.198.195:8080/api/author/v1/login',
          data: userLoginParams.toJson());

      if (response != null) {
        User user = User.fromJson(response.data['data']);
        Session.instance().setAccessToken(user.token);
        Session.instance().setUserID(user.userId);
        Session.instance().setUserRole(user.role);
        Session.instance().setUserName(user.name);
      }
    } catch (e) {
      throw e;
    }
  }

  @override
  Future<void> createClass(ClassCreateParams classCreateParams) async {
    try {
      (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient client) {
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      };

      await _dio.post(
        'http://35.240.198.195:8080/api/management/clazz',
        options: Options(
          headers: {
            'Authorization': "Bearer " + Session.instance().accessToken,
            'user_id': Session.instance().userID, // set content-length
          },
        ),
        data: classCreateParams.toJson(),
      );
    } catch (e) {
      throw e;
    }
  }

  @override
  Future<void> joinClass(String params) async {
    try {
      (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient client) {
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      };
      String path =
          'http://35.240.198.195:8080/api/test/exam/clazz/join?code={code}'
              .replaceAll("{code}", params ?? "");
      await _dio.get(
        path,
        options: Options(
          headers: {
            'Authorization': "Bearer " + Session.instance().accessToken,
            'user_id': Session.instance().userID,
          },
        ),
      );
    } catch (e) {
      throw e;
    }
  }

  @override
  Future<Data> joinTest(String params) async {
    try {
      (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient client) {
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      };
      String path =
          'http://35.240.198.195:8080/api/test/exam/{code}'
              .replaceAll("{code}", params ?? "");
      var response = await _dio.get(
        path,
        options: Options(
          headers: {
            'Authorization': "Bearer " + Session.instance().accessToken,
            'user_id': Session.instance().userID,
          },
        ),
      );

      if(response != null){
        Data test = Data.fromJson(response.data['data']);
        return test;
      }
    } catch (e) {
      throw e;
    }
  }
}
