import 'package:get_it/get_it.dart';
import 'package:mobile_exercise/base/bloc_base.dart';
import 'package:mobile_exercise/data/models/user_login_params.dart';
import 'package:mobile_exercise/data/repository/account_repository.dart';
import 'package:mobile_exercise/global/app_navigation.dart';
import 'package:mobile_exercise/global/app_routes.dart';
import 'package:mobile_exercise/global/app_toast.dart';
import 'package:rxdart/subjects.dart';

class LoginBloc extends BlocBase {
  final AccountRepositoryIml _apiAccount = AccountRepositoryIml();

  @override
  void init() async {
    super.init();
  }

  final BehaviorSubject<String> _userNameValue = BehaviorSubject();
  Sink<String> get userNameValueSink => _userNameValue.sink;
  Stream<String> get userNameValueStream => _userNameValue.stream;

  final BehaviorSubject<String> _passwordValue = BehaviorSubject();
  Sink<String> get passwordValueSink => _passwordValue.sink;
  Stream<String> get passwordValueSteam => _passwordValue.stream;

  Future<void> login() async {
    try {
      UserLoginParams userSignUpParams = UserLoginParams(
        username: _userNameValue.value,
        password: _passwordValue.value,
      );
      await _apiAccount.login(userSignUpParams);
      AppToast.showSuccess('Đăng kí thành công, hãy xác tài khoản của bạn');
      GetIt.I<Navigation>().pushReplacementNamed(AppRouter.mainPage);
    } catch (e) {
      AppToast.showError('Đã xảy ra lỗi');
    }
  }

  @override
  void dispose() {
    _userNameValue.close();
    _passwordValue.close();
  }
}
