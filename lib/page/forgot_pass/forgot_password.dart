import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:mobile_exercise/base/widget/button.dart';
import 'package:mobile_exercise/global/app_color.dart';
import 'package:mobile_exercise/global/app_navigation.dart';
import 'package:mobile_exercise/global/app_path.dart';
import 'package:mobile_exercise/global/app_routes.dart';

class ForgotPass extends StatefulWidget {
  @override
  _ForgotPassState createState() => _ForgotPassState();
}

class _ForgotPassState extends State<ForgotPass> {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passController = TextEditingController();
  GlobalKey<FormState> _key = GlobalKey();
  bool _obscureText = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: AppColors.white,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          centerTitle: true,
          title: Text('Quên mật khẩu',style: TextStyle(color: Colors.black),),
          leading: InkWell(
            onTap: () {
              GetIt.I<Navigation>().pop();
            },
            child: Icon(Icons.arrow_back_ios,color: Colors.black,),
          ),
        ),
        body: _buildBody(),
      ),
    );
  }

  Widget _buildBody() {
    return Form(
      key: _key,
      child: ListView(
        children: [
          SizedBox(height: 50),
          Container(
              height: MediaQuery.of(context).size.width / 2,
              child: Image(
                image: AssetImage(AppPath.loading),
              )),
          Padding(
            padding: EdgeInsets.only(left: 48, right: 48, bottom: 15),
            child: Align(
              alignment: Alignment.center,
              child: GestureDetector(
                child: Text(
                  'Hệ Thống Quản Lý Học Tập',
                  style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.w700,
                      color: AppColors.black),
                ),
              ),
            ),
          ),
          SizedBox(height: 27),
          phone(),
          loginButton(),
        ],
      ),
    );
  }

  Widget phone() {
    return Padding(
      padding: EdgeInsets.only(left: 48, right: 48, bottom: 13),
      child: Container(
        height: 40,
        child: TextFormField(
          decoration: InputDecoration(
              hintText: 'Số điện thoại',
              contentPadding: EdgeInsets.only(bottom: 10, left: 15),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
              )),
          onChanged: (value) {},
        ),
      ),
    );
  }


  Widget loginButton() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 70),
      child: CustomButton(
        height: 40,
        width: 102,
        title: 'Gửi',
        color: AppColors.blue,
        titleColor: AppColors.white,
        onPress: () async {
          GetIt.I<Navigation>().pushNamed(AppRouter.sinCodeVerificationScreen);
        },
      ),
    );
  }
}
