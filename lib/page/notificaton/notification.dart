import 'package:flutter/material.dart';
import 'package:mobile_exercise/global/app_color.dart';
import 'package:mobile_exercise/global/app_toast.dart';

class Notifications extends StatefulWidget{
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications>{
  @override
  void initState() {
    super.initState();
    AppToast.showWarning('Tính năng đang phát triển');
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Text('Tính năng đang phát triển!',style: TextStyle(color: AppColors.red,fontSize: 20,fontWeight: FontWeight.w700),),
        )
    );
  }
}