import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile_exercise/global/app_color.dart';
import 'package:mobile_exercise/instance/app_index.dart';
import 'package:mobile_exercise/page/home/home.dart';
import 'package:mobile_exercise/page/main/widget/navy_bottom_bar.dart';
import 'package:mobile_exercise/page/my_task/manager_history_task.dart';
import 'package:mobile_exercise/page/notificaton/notification.dart';
import 'package:mobile_exercise/page/settings/settings.dart';
import 'package:provider/provider.dart';


class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: 0);
    _bind();
  }

  void _bind() {
    AppIndex.instance().addListener(() {
      try {
        _pageController?.jumpToPage(AppIndex.instance().index ?? 0);
      } catch (_) {}
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        body: _buildPageView(),
        bottomNavigationBar: _buildNavy(),
      ),
      onWillPop: () async => false,
    );
  }

  Widget _buildNavy() {
    return BottomNavyBar(
      selectedIndex: context.watch<AppIndex>().index,
      showElevation: true, // use this to remove appBar's elevation
      onItemSelected: (index) {
        AppIndex.instance().updateIndex(index);
      },
      items: [
        BottomNavyBarItem(
            icon: Icon(Icons.home_outlined),
            title: 'Trang chủ',
            activeColor: AppColors.blueDiamond,
            inactiveColor: AppColors.grey),
        BottomNavyBarItem(
            icon: Icon(Icons.notification_important_outlined),
            title: 'Thông Báo',
            activeColor: AppColors.blueDiamond,
            inactiveColor: AppColors.grey),
        BottomNavyBarItem(
            icon: Icon(Icons.pending_actions),
            title: 'Hoạt Động',
            activeColor: AppColors.blueDiamond,
            inactiveColor: AppColors.grey),
        BottomNavyBarItem(
            icon: Icon(Icons.account_box_outlined),
            title: 'Tài Khoản',
            activeColor: AppColors.blueDiamond,
            inactiveColor: AppColors.grey),
      ],
    );
  }


  Widget _buildPageView() {
    return PageView(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      children: [
        MyHomePage(),
        MyTasks(),
        Notifications(),
        Settings(),
      ]
    );
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }
}
