import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:mobile_exercise/base/widget/dialog.dart';
import 'package:mobile_exercise/base/widget/diglog_confirm.dart';
import 'package:mobile_exercise/global/app_color.dart';
import 'package:mobile_exercise/global/app_demension.dart';
import 'package:mobile_exercise/global/app_navigation.dart';
import 'package:mobile_exercise/instance/Session.dart';

class ClassStudy extends StatefulWidget {
  @override
  _ClassStudyState createState() => _ClassStudyState();
}

class _ClassStudyState extends State<ClassStudy> {
  bool checkMicTeacher = true;

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: AppColors.white,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          centerTitle: true,
          title: Text(
            'Lớp học',
            style: TextStyle(color: Colors.black),
          ),
          leading: InkWell(
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) => BuBuAlertDialog(
                  title: 'Cảnh báo',
                  des: 'Bạn có chắc chắn muốn thoát khỏi lớp học chứ?',
                  accept: () {
                    _timer.cancel();
                    GetIt.I<Navigation>().pop();
                    GetIt.I<Navigation>().pop();
                  },
                  actionCancel: () {
                    GetIt.I<Navigation>().pop();
                  },
                  cancelText: 'Không',
                  acceptText: 'Đồng ý',
                ),
              );
            },
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
          ),
        ),
        body: _buildBody(),
      ),
    );
  }

  Widget _buildBody() {
    return Padding(
      padding: EdgeInsets.all(15),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                'Thời gian: ',
                style: TextStyle(
                    color: AppColors.blueDiamond,
                    fontSize: 14,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(width: 10),
              Text('$hours: $minutes: $seconds'),
              Spacer(),
              InkWell(
                onTap: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) => AlertDialogCustom(
                      title: 'Thông báo',
                      des: 'Tính năng này chỉ sử đụng được trên Website',
                      actionCancel: () {
                        GetIt.I<Navigation>().pop();
                      },
                      cancelText: 'Đóng',
                    ),
                  );
                },
                child: Icon(
                  Icons.message_outlined,
                  color: AppColors.blueDiamond,
                ),
              ),
              SizedBox(width: 10),
              InkWell(
                onTap: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) => AlertDialogCustom(
                      title: 'Thông báo',
                      des: 'Tính năng này chưa phát triển',
                      actionCancel: () {
                        GetIt.I<Navigation>().pop();
                      },
                      cancelText: 'Đóng',
                    ),
                  );
                },
                child: Icon(
                  Icons.add,
                  color: AppColors.blueDiamond,
                ),
              ),
            ],
          ),
          SizedBox(height: 20),
          Container(
            height: 60,
            width: getWidth(context) * 0.7,
            padding: const EdgeInsets.all(3.0),
            decoration:
                BoxDecoration(border: Border.all(color: Colors.blueAccent)),
            child: Row(
              children: [
                SizedBox(width: 10),
                Visibility(
                  visible: Session.instance().userRole == "1",
                  child: Text('Giáo viên: ${Session.instance().userName}'),
                ),
                Visibility(
                  visible: Session.instance().userRole == "0",
                  child: Text('Giáo viên: Nguyễn Tiến Anh'),
                ),
                Spacer(),
                InkWell(
                  onTap: () {
                    setState(() {
                      checkMicTeacher = !checkMicTeacher;
                    });
                  },
                  child:
                      checkMicTeacher ? Icon(Icons.mic) : Icon(Icons.mic_off),
                ),
                SizedBox(width: 10),
              ],
            ),
          ),
          SizedBox(height: 20),
          Flexible(
            child: ListView.builder(
              itemCount: 10,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.only(bottom: 15),
                  height: 60,
                  width: getWidth(context) * 0.7,
                  padding: const EdgeInsets.all(3.0),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.blueAccent)),
                  child: Row(
                    children: [
                      InkWell(
                        onTap: () {},
                        child: Icon(Icons.close),
                      ),
                      SizedBox(width: 20),
                      Text('Nguyễn $index'),
                      Spacer(),
                      InkWell(
                        onTap: () {},
                        child: checkMicTeacher
                            ? Icon(Icons.mic)
                            : Icon(Icons.mic_off),
                      ),
                      SizedBox(width: 10),
                    ],
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }

  Timer _timer;
  int seconds = 0;
  int minutes = 0;
  int hours = 0;
  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (seconds < 0) {
            timer.cancel();
          } else {
            seconds = seconds + 1;
            if (seconds > 59) {
              minutes += 1;
              seconds = 0;
              if (minutes > 59) {
                hours += 1;
                minutes = 0;
              }
            }
          }
        },
      ),
    );
  }
}
