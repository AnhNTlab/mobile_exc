import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:mobile_exercise/base/widget/dialog.dart';
import 'package:mobile_exercise/global/app_color.dart';
import 'package:mobile_exercise/global/app_navigation.dart';
import 'package:mobile_exercise/global/app_toast.dart';

class Settings extends StatefulWidget {
  const Settings({Key key}) : super(key: key);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Text('Tính năng đang phát triển!',style: TextStyle(color: AppColors.red,fontSize: 20,fontWeight: FontWeight.w700),),
        )
    );
  }
}
