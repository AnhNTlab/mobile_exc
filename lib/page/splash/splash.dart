import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:mobile_exercise/global/app_navigation.dart';
import 'package:mobile_exercise/global/app_path.dart';
import 'package:mobile_exercise/global/app_routes.dart';


class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with TickerProviderStateMixin{
  final int splashDuration = 3;
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              height: MediaQuery.of(context).size.width,
              child: Image(
                image: AssetImage(AppPath.loading),
              )
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 100),
            child: Row(
              children: [
                 RaisedButton(
                  onPressed: () => {
                    GetIt.I<Navigation>().pushNamed(AppRouter.login)
                  },
                  child: Text('Login'),
                  //other properties
                ),
                 Spacer(),
                 RaisedButton(
                  onPressed: () => {
                    GetIt.I<Navigation>().pushNamed(AppRouter.signUp)
                  },
                  child: Text('SignUp'),
                  //other properties
                ),
              ],
            ),
          )
        ],
      )
    );
  }
}
