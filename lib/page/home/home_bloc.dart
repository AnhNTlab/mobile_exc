import 'package:get_it/get_it.dart';
import 'package:mobile_exercise/base/bloc_base.dart';
import 'package:mobile_exercise/data/models/class%20param.dart';
import 'package:mobile_exercise/data/repository/account_repository.dart';
import 'package:mobile_exercise/domain/models/test.dart';
import 'package:mobile_exercise/global/app_navigation.dart';
import 'package:mobile_exercise/global/app_routes.dart';
import 'package:mobile_exercise/global/app_toast.dart';
import 'package:rxdart/subjects.dart';

class HomeBloc extends BlocBase {
  final AccountRepositoryIml _apiAccount = AccountRepositoryIml();

  @override
  void init() async {
    super.init();
  }

  final BehaviorSubject<String> _className = BehaviorSubject();
  Sink<String> get classNameValueSink => _className.sink;
  Stream<String> get classNameValueStream => _className.stream;

  final BehaviorSubject<String> _amount = BehaviorSubject();
  Sink<String> get amountSink => _amount.sink;
  Stream<String> get amountStream => _amount.stream;

  final BehaviorSubject<String> _classNameJoin = BehaviorSubject();
  Sink<String> get classNameJoinValueSink => _classNameJoin.sink;
  Stream<String> get classNameJoinValueStream => _classNameJoin.stream;

  final BehaviorSubject<String> _classNameJoinTest = BehaviorSubject();
  Sink<String> get classNameJoinTestSink => _classNameJoinTest.sink;
  Stream<String> get classNameJoinTestStream => _classNameJoinTest.stream;

  final BehaviorSubject<Data> _test = BehaviorSubject();
  Sink<Data> get testSink => _test.sink;
  Stream<Data> get testStream => _test.stream;

  Future<void> createClass() async {
    try {
      ClassCreateParams classCreateParams = ClassCreateParams(
          description: _amount.value,
          name: _className.value,
      );
      await _apiAccount.createClass(classCreateParams);
      AppToast.showSuccess('Tạo lớp học thành công');
      GetIt.I<Navigation>().pop();
      GetIt.I<Navigation>().pushNamed(AppRouter.classStudy);
    } catch (e) {
      AppToast.showError('Đã xảy ra lỗi');
    }
  }

  Future<void> joinClass() async {
    try {
      await _apiAccount.joinClass(_classNameJoin.value);
      AppToast.showSuccess('Vào lớp học thành công');
      GetIt.I<Navigation>().pop();
      GetIt.I<Navigation>().pushNamed(AppRouter.classStudy);
    } catch (e) {
      AppToast.showError('Đã xảy ra lỗi');
    }
  }

  Future<void> joinTest() async {
    try {
      Data test = await _apiAccount.joinTest(_classNameJoinTest.value);
      if(test == null){
        return;
      }
      print(test.questionDtoList[0]);
      _test.add(test);
      AppToast.showSuccess('Bắt đầu làm bài kiểm tra');
      GetIt.I<Navigation>().pop();
      GetIt.I<Navigation>().pushNamed(AppRouter.test,arguments: _test.value);
    } catch (e) {
      AppToast.showError('Đã xảy ra lỗi');
    }
  }


  @override
  void dispose() {
    _className.close();
    _amount.close();
    _classNameJoin.close();
    _classNameJoinTest.close();
    _test.close();
  }
}
