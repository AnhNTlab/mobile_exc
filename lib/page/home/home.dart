import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:mobile_exercise/base/widget/button.dart';
import 'package:mobile_exercise/base/widget/dialog.dart';
import 'package:mobile_exercise/global/app_color.dart';
import 'package:mobile_exercise/global/app_navigation.dart';
import 'package:mobile_exercise/instance/Session.dart';
import 'package:mobile_exercise/page/home/home_bloc.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  HomeBloc _bloc;
  @override
  void initState() {
    super.initState();
    _bloc = HomeBloc()..init();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
          body: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
              onTap: () {
                print(Session.instance().userRole);
                Session.instance().userRole == "1" ? showBarModalBottomSheet(
                  context: context,
                  builder: (context) => Container(
                    height: MediaQuery.of(context).size.height*0.8,
                    child: Column(
                      children: [
                        SizedBox(height: 30),
                        Container(
                          height: 20,
                          child: Text(
                            'Tạo lớp học',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                        Container(
                          height: 50,
                          child: Text('Tên giáo viên: Nguyễn Tiến Anh'),
                        ),
                        SizedBox(height: 15),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 15),
                          child: TextFormField(
                            onChanged: (value){
                              _bloc.classNameValueSink.add(value);
                            },
                            decoration: new InputDecoration(
                              border: new OutlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Colors.teal)),
                              hintText: 'Tên/Mã lớp',
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 15),
                          child: TextFormField(
                            onChanged: (value){
                              _bloc.amountSink.add(value);
                            },
                            decoration: new InputDecoration(
                              border: new OutlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Colors.teal)),
                              hintText: 'Số lượng học sinh',
                            ),
                          ),
                        ),
                        SizedBox(height: 60),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 15),
                          child: CustomButton(
                            height: 40,
                            width: 200,
                            title: 'Tạo lớp học',
                            color: AppColors.blue,
                            titleColor: AppColors.white,
                            onPress: () async {
                              await _bloc.createClass();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ): showDialog(
                  context: context,
                  builder: (BuildContext context) => AlertDialogCustom(
                    title: 'Thông báo',
                    des: 'Chức năng này chỉ giáo viên mới có thể sử dụng',
                    actionCancel: () {
                      GetIt.I<Navigation>().pop();
                    },
                    cancelText: 'Đóng',
                  ),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(
                            5.0) //                 <--- border radius here
                        ),
                    color: AppColors.blueDiamond),
                height: 60,
                child: Center(
                    child: Text(
                  'Tạo lớp học',
                  style: TextStyle(
                      color: AppColors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                )),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            InkWell(
              onTap: () {
                Session.instance().userRole == "0" ? showBarModalBottomSheet(
                  context: context,
                  builder: (context) => Container(
                    height: MediaQuery.of(context).size.height*0.8,
                    child: Column(
                      children: [
                        SizedBox(height: 30),
                        Container(
                          height: 20,
                          child: Text(
                            'Tham gia lớp học',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                        Container(
                          height: 50,
                          child: Text('Tên học viên: ${Session.instance().userName}'),
                        ),
                        SizedBox(height: 15),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 15),
                          child: TextFormField(
                            onChanged: (value){
                              _bloc.classNameJoinValueSink.add(value);
                            },
                            decoration: new InputDecoration(
                              border: new OutlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Colors.teal)),
                              hintText: 'Tên/Mã lớp',
                            ),
                          ),
                        ),
                        SizedBox(height: 60),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 15),
                          child: CustomButton(
                            height: 40,
                            width: 200,
                            title: 'Tham gia lớp học',
                            color: AppColors.blue,
                            titleColor: AppColors.white,
                            onPress: () async {
                              _bloc.joinClass();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ) : showDialog(
                  context: context,
                  builder: (BuildContext context) => AlertDialogCustom(
                    title: 'Thông báo',
                    des: 'Chức năng này chỉ học sinh mới có thể sử dụng',
                    actionCancel: () {
                      GetIt.I<Navigation>().pop();
                    },
                    cancelText: 'Đóng',
                  ),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(
                            5.0) //                 <--- border radius here
                        ),
                    color: AppColors.blueDiamond),
                height: 60,
                child: Center(
                    child: Text(
                  'Tham gia lớp học',
                  style: TextStyle(
                      color: AppColors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                ),
                ),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            InkWell(
              onTap: (){
                Session.instance().userRole == "0" ? showBarModalBottomSheet(
                  context: context,
                  builder: (context) => Container(
                    height: MediaQuery.of(context).size.height*0.8,
                    child: Column(
                      children: [
                        SizedBox(height: 30),
                        Container(
                          height: 20,
                          child: Text(
                            'Làm bài kiểm tra',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                        Container(
                          height: 50,
                          child: Text('Tên học viên: ${Session.instance().userName}'),
                        ),
                        SizedBox(height: 15),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 15),
                          child: TextFormField(
                            onChanged: (value){
                              _bloc.classNameJoinTestSink.add(value);
                            },
                            decoration: new InputDecoration(
                              border: new OutlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Colors.teal)),
                              hintText: 'Tên/Mã bài kiểm tra',
                            ),
                          ),
                        ),
                        SizedBox(height: 60),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 15),
                          child: CustomButton(
                            height: 40,
                            width: 200,
                            title: 'Bắt đầu làm bài',
                            color: AppColors.blue,
                            titleColor: AppColors.white,
                            onPress: () async {
                              _bloc.joinTest();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ) : showDialog(
                  context: context,
                  builder: (BuildContext context) => AlertDialogCustom(
                    title: 'Thông báo',
                    des: 'Chức năng này chỉ học sinh mới có thể sử dụng',
                    actionCancel: () {
                      GetIt.I<Navigation>().pop();
                    },
                    cancelText: 'Đóng',
                  ),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(
                            5.0) //                 <--- border radius here
                        ),
                    color: AppColors.blueDiamond),
                height: 60,
                child: Center(
                    child: Text(
                  'Làm bài kiểm tra',
                  style: TextStyle(
                      color: AppColors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                )),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            InkWell(
              onTap: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => AlertDialogCustom(
                    title: 'Thông báo',
                    des: 'Tính năng này chỉ sử đụng được trên Website',
                    actionCancel: () {
                      GetIt.I<Navigation>().pop();
                    },
                    cancelText: 'Đóng',
                  ),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(
                            5.0) //                 <--- border radius here
                        ),
                    color: AppColors.blueDiamond),
                height: 60,
                child: Center(
                    child: Text(
                  'Tạo bài kiểm tra',
                  style: TextStyle(
                      color: AppColors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                )),
              ),
            )
          ],
        ),
      )),
    );
  }
}
