import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:mobile_exercise/base/widget/diglog_confirm.dart';
import 'package:mobile_exercise/domain/models/test.dart';
import 'package:mobile_exercise/global/app_color.dart';
import 'package:mobile_exercise/global/app_demension.dart';
import 'package:mobile_exercise/global/app_navigation.dart';
import 'package:mobile_exercise/global/app_routes.dart';
import 'package:mobile_exercise/page/test/test_bloc.dart';

class Test extends StatefulWidget {
  final Data test;

  Test({this.test});
  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Test> {

  TestBloc _bloc;

  Timer _timer;
  int _start;

  void startTimer() {
     _start =  widget.test.timeLimit;
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
          (Timer timer) {
        if (_start == 0) {
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  @override
  void initState() {
    startTimer();
    super.initState();
  }


  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
          elevation: 0.0,
          child: Container(
            child:   Icon(Icons.subdirectory_arrow_right_rounded),
          ),
          backgroundColor: AppColors.blueDiamond,
          onPressed: (){
            showDialog(
              context: context,
              builder: (BuildContext context) => BuBuAlertDialog(
                title: 'Xác nhận',
                des: 'Bạn có chắc chắn muốn nộp bài không',
                accept: () {
                  _timer.cancel();
                  GetIt.I<Navigation>().pop();
                  GetIt.I<Navigation>().pushNamed(AppRouter.result);
                },
                actionCancel: () {
                  GetIt.I<Navigation>().pop();
                },
                cancelText: 'Không',
                acceptText: 'Đồng ý',
              ),
            );
          }
      ),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          'Bài Kiểm Tra',
          style: TextStyle(color: AppColors.blueDiamond),
        ),
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_ios,
            color: AppColors.blueDiamond,
          ),
        ),
        actions: [
          Center(child: Text("$_start",style: TextStyle(fontSize: 16,color: AppColors.red),)),
          SizedBox(width: 20,)
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: ListView.builder(
          itemCount: widget.test.questionDtoList.length,
          itemBuilder: (context,index){
            var question = widget.test.questionDtoList;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Câu ${index + 1}',style: TextStyle(color: AppColors.blue,fontWeight: FontWeight.bold,fontSize: 14),),
                SizedBox(height: 20),
                Text('${question[index].name}',style: TextStyle(color: AppColors.black,fontWeight: FontWeight.normal,fontSize: 14),),
                SizedBox(height: 30),
                Container(
                  height: 280,
                  child: ListView.builder(
                    itemCount: widget.test.questionDtoList[index].answerDtos.length,
                    itemBuilder: (context,i){
                      return InkWell(
                        onTap: (){},
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          margin: EdgeInsets.only(bottom: 10),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black)
                          ),
                          height: 50,
                          child: Row(
                            children: [
                              Text('${i + 1}',style: TextStyle(fontSize: 16),),
                              SizedBox(width: 10),
                              Text('${widget.test.questionDtoList[index].answerDtos[i].name}',style: TextStyle(fontSize: 14),),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
