import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:mobile_exercise/domain/models/test.dart';
import 'package:mobile_exercise/global/app_color.dart';
import 'package:mobile_exercise/global/app_navigation.dart';
import 'package:mobile_exercise/global/app_routes.dart';
import 'package:mobile_exercise/instance/app_index.dart';
import 'package:mobile_exercise/page/test/test_bloc.dart';

class Result extends StatefulWidget {
  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Result> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('Chúc mừng bạn đã hoàn thành bài thi', style: TextStyle(color: AppColors.black)),
            SizedBox(height: 10),
            Text('Kết quả của bạn: ?/? câu', style: TextStyle(color: AppColors.red)),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50,vertical: 20),
              child: InkWell(
                onTap: () {
                  AppIndex.instance().updateIndex(0);
                  GetIt.I<Navigation>().pushReplacementNamed(AppRouter.mainPage);
                },
                child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      color: AppColors.blueDiamond),
                  child: Center(
                      child: Text(
                    'Quay về trang chủ',
                    style: TextStyle(color: AppColors.white),
                  )),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
