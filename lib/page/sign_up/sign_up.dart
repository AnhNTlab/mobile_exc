import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:mobile_exercise/base/widget/button.dart';
import 'package:mobile_exercise/global/app_color.dart';
import 'package:mobile_exercise/global/app_navigation.dart';
import 'package:mobile_exercise/global/app_path.dart';
import 'package:mobile_exercise/global/app_routes.dart';
import 'package:mobile_exercise/page/sign_up/sign_up_bloc.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passController = TextEditingController();
  GlobalKey<FormState> _key = GlobalKey();
  bool _obscureText = true;
  bool _obscureTexts = true;
  SignUpBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = SignUpBloc()..init();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: AppColors.white,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          centerTitle: true,
          title: Text(
            'Đăng nhập',
            style: TextStyle(color: Colors.black),
          ),
          leading: InkWell(
            onTap: () {
              GetIt.I<Navigation>().pop();
            },
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
          ),
        ),
        body: _buildBody(),
      ),
    );
  }

  Widget _buildBody() {
    return Form(
      key: _key,
      child: ListView(
        children: [
          SizedBox(height: 50),
          Container(
              height: MediaQuery.of(context).size.width / 2,
              child: Image(
                image: AssetImage(AppPath.loading),
              )),
          Padding(
            padding: EdgeInsets.only(left: 48, right: 48, bottom: 15),
            child: Align(
              alignment: Alignment.center,
              child: GestureDetector(
                child: Text(
                  'Hệ Thống Quản Lý Học Tập',
                  style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.w700,
                      color: AppColors.black),
                ),
              ),
            ),
          ),
          SizedBox(height: 45),
          SizedBox(height: 27),
          Name(),
          userName(),
          phone(),
          passWord(),
          passWordConfirm(),
          loginButton(),
          SizedBox(height: 45),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 100),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Đã có tài khoản',
                    style: TextStyle(fontSize: 14, color: AppColors.black)),
                InkWell(
                  onTap: (){
                    GetIt.I<Navigation>().pushNamed(AppRouter.login);
                  },
                  child: Text(
                    'Đăng nhập',
                    style: TextStyle(fontSize: 14, color: AppColors.blue),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 45),
        ],
      ),
    );
  }

  Widget Name() {
    return Padding(
      padding: EdgeInsets.only(left: 48, right: 48, bottom: 13),
      child: Container(
        height: 33,
        child: TextFormField(
          decoration: InputDecoration(
              hintText: 'Họ và tên',
              contentPadding: EdgeInsets.only(bottom: 10, left: 15),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
              )),
          onChanged: (value) {
            _bloc.nameSink.add(value);
          },
        ),
      ),
    );
  }

  Widget userName() {
    return Padding(
      padding: EdgeInsets.only(left: 48, right: 48, bottom: 13),
      child: Container(
        height: 33,
        child: TextFormField(
          decoration: InputDecoration(
              hintText: 'Tên Đăng Nhập',
              contentPadding: EdgeInsets.only(bottom: 10, left: 15),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
              )),
          onChanged: (value) {
            _bloc.userNameValueSink.add(value);
          },
        ),
      ),
    );
  }

  Widget phone() {
    return Padding(
      padding: EdgeInsets.only(left: 48, right: 48, bottom: 13),
      child: Container(
        height: 33,
        child: TextFormField(
          decoration: InputDecoration(
              hintText: 'Email hoặc SĐT',
              contentPadding: EdgeInsets.only(bottom: 10, left: 15),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
              )),
          onChanged: (value) {
            _bloc.emailValueSink.add(value);
          },
        ),
      ),
    );
  }

  Widget passWord() {
    return Padding(
      padding: EdgeInsets.only(left: 48, right: 48, bottom: 20),
      child: Container(
        height: 33,
        child: TextField(
          obscureText: _obscureText,
          decoration: InputDecoration(
              hintText: 'Mật khẩu',
              contentPadding: EdgeInsets.only(bottom: 10, left: 15),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
              ),
              suffixIcon: InkWell(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onTap: () {
                  setState(() {
                    _obscureText = !_obscureText;
                  });
                },
                child: Icon(Icons.remove_red_eye),
              )),
          onChanged: (value) {
            _bloc.passwordValueSink.add(value);
          },
        ),
      ),
    );
  }

  Widget passWordConfirm() {
    return Padding(
      padding: EdgeInsets.only(left: 48, right: 48, bottom: 20),
      child: Container(
        height: 33,
        child: TextField(
          obscureText: _obscureText,
          decoration: InputDecoration(
              hintText: 'Xác nhận mật khẩu mật khẩu',
              contentPadding: EdgeInsets.only(bottom: 10, left: 15),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
              ),
              suffixIcon: InkWell(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onTap: () {
                  setState(() {
                    _obscureTexts = !_obscureTexts;
                  });
                },
                child: Icon(Icons.remove_red_eye),
              )),
          onChanged: (value) {
            _bloc.confirmPasswordValueSink.add(value);
          },
        ),
      ),
    );
  }

  Widget loginButton() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 70),
      child: CustomButton(
        height: 33,
        width: 102,
        title: 'Đăng Ký',
        color: AppColors.blue,
        titleColor: AppColors.white,
        onPress: () async {
          _bloc.signUp();
        },
      ),
    );
  }
}
