import 'package:get_it/get_it.dart';
import 'package:mobile_exercise/base/bloc_base.dart';
import 'package:mobile_exercise/data/models/user_signUP_params.dart';
import 'package:mobile_exercise/data/repository/account_repository.dart';
import 'package:mobile_exercise/global/app_navigation.dart';
import 'package:mobile_exercise/global/app_routes.dart';
import 'package:mobile_exercise/global/app_toast.dart';
import 'package:rxdart/subjects.dart';

class SignUpBloc extends BlocBase {
  final AccountRepositoryIml _apiAccount = AccountRepositoryIml();

  @override
  void init() async {
    super.init();
  }

  final BehaviorSubject<String> _userNameValue = BehaviorSubject();
  Sink<String> get userNameValueSink => _userNameValue.sink;
  Stream<String> get userNameValueStream => _userNameValue.stream;

  final BehaviorSubject<String> _name = BehaviorSubject();
  Sink<String> get nameSink => _name.sink;
  Stream<String> get nameStream => _name.stream;

  final BehaviorSubject<String> _emailValue = BehaviorSubject();
  Sink<String> get emailValueSink => _emailValue.sink;
  Stream<String> get emailValueSteam => _emailValue.stream;

  final BehaviorSubject<String> _passwordValue = BehaviorSubject();
  Sink<String> get passwordValueSink => _passwordValue.sink;
  Stream<String> get passwordValueSteam => _passwordValue.stream;

  final BehaviorSubject<String> _confirmPasswordValue = BehaviorSubject();
  Sink<String> get confirmPasswordValueSink => _confirmPasswordValue.sink;
  Stream<String> get confirmPasswordValueSteam => _confirmPasswordValue.stream;

  Future<void> signUp() async {
    try {
      UserSignUpParams userSignUpParams = UserSignUpParams(
          emailOrPhoneNumber: _emailValue.value,
          password: _passwordValue.value,
          role: '0',
          name: _name.value,
          userName: _userNameValue.value);
      await _apiAccount.signUP(userSignUpParams);
      GetIt.I<Navigation>().pushNamed(AppRouter.sinCodeVerificationScreen,
          arguments: _emailValue.value);
      AppToast.showSuccess('Đăng kí thành công, hãy xác tài khoản của bạn');
    } catch (e) {
      AppToast.showError('Đã xảy ra lỗi');
    }
  }

  @override
  void dispose() {
    _userNameValue.close();
    _emailValue.close();
    _passwordValue.close();
    _confirmPasswordValue.close();
    _name.close();
  }
}
