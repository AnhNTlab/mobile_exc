import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CustomButton extends StatefulWidget {
  final Widget child;
  final double height;
  final double width;
  final GestureTapCallback onPress;
  final String title;
  final Color color;
  final Color titleColor;

  const CustomButton({Key key, this.height, this.width, this.child, this.onPress, this.title, this.color, this.titleColor}) : super(key: key);
  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onPress,
      child: Container(
          decoration: BoxDecoration(
            color: widget.color??Colors.white,
            borderRadius: BorderRadius.circular(10),
          ),
          height: widget.height??36,
          width: widget.width??92,
          child: Center(
            child: Text(
                widget.title??"",
                style: TextStyle(fontSize: 12,
                    fontWeight: FontWeight.w700,
                    color: widget.titleColor??Colors.black),
            ),
          )
      ),
    );
  }
}
