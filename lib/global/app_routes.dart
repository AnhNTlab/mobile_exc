import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile_exercise/page/class_study/class_study.dart';
import 'package:mobile_exercise/page/forgot_pass/forgot_password.dart';
import 'package:mobile_exercise/page/home/home.dart';
import 'package:mobile_exercise/page/login/login.dart';
import 'package:mobile_exercise/page/main/main_page.dart';
import 'package:mobile_exercise/page/otp/otp.dart';
import 'package:mobile_exercise/page/result/resullt.dart';
import 'package:mobile_exercise/page/sign_up/sign_up.dart';
import 'package:mobile_exercise/page/splash/splash.dart';
import 'package:mobile_exercise/page/test/test.dart';

class AppRouter {
  AppRouter._();

  static MaterialPageRoute<Widget> onGenerateRoute(RouteSettings settings) {
    return MaterialPageRoute<Widget>(
      builder: (BuildContext context) => makeRoute(
          context: context,
          routeName: settings.name,
          arguments: settings.arguments),
    );
  }

  static Widget makeRoute(
      {@required BuildContext context,
      @required String routeName,
      Object arguments}) {
    switch (routeName) {
      case splash:
        return SplashScreen();
      case mainPage:
        return MainPage();
      case homePage:
        return MyHomePage();
        break;
      case login:
        return Login();
        break;
      case signUp:
        return SignUp();
      case forgot:
        return ForgotPass();
      case sinCodeVerificationScreen:
        return PinCodeVerificationScreen(
          phoneNumber: arguments,
        );
      case classStudy:
        return ClassStudy();
      case test:
        return Test(
          test: arguments,
        );
        case result:
        return Result();
      default:
        throw 'Route $routeName is not defined';
    }
  }

  static const String splash = '/splash';
  static const String homePage = '/homePage';
  static const String login = '/login';
  static const String postTask = '/postTask';
  static const String mainPage = '/';
  static const String signUp = '/signUp';
  static const String qrCodeScan = '/qrCodeScan';
  static const String updateProfile = '/updateProfile';
  static const String tutorialQR = '/tutorialQR';
  static const String detailsTask = '/detailsTask';
  static const String addAddress = '/addAddress';
  static const String myProfile = '/myProfile';
  static const String pickAddress = '/pickAddress';
  static const String forgot = '/forgot';
  static const String sinCodeVerificationScreen = '/sinCodeVerificationScreen';
  static const String classStudy = '/classStudy';
  static const String test = '/test';
  static const String result = '/result';
}
