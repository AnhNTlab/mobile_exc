import 'package:flutter/material.dart';

class Navigation {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  bool closing = false;

  Future<T> pushNamed<T extends Object>(String routeName, {Object arguments}) {
    return navigatorKey.currentState.pushNamed(routeName, arguments: arguments);
  }

  Future<dynamic> pushReplacementNamed(String routeName, {Object arguments}) {
    return navigatorKey.currentState
        .pushReplacementNamed(routeName, arguments: arguments);
  }

  Future<dynamic> push(Widget widget, {bool visible = true}) {
    return navigatorKey.currentState.push(
      MaterialPageRoute(
        builder: (context) => widget,
      ),
    );
  }

  void pop<T extends Object>({T result}) {
    navigatorKey.currentState.pop(result);
  }

}
