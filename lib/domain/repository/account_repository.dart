
import 'package:mobile_exercise/data/models/class%20param.dart';
import 'package:mobile_exercise/data/models/user_login_params.dart';
import 'package:mobile_exercise/data/models/user_signUP_params.dart';
import 'package:mobile_exercise/domain/models/test.dart';

abstract class AccountRepository{
  Future<void> signUP(UserSignUpParams userSignUpParams);
  Future<void> login(UserLoginParams userLoginParams);
  Future<void> createClass(ClassCreateParams classCreateParams);
  Future<void> joinClass(String params);
  Future<Data> joinTest(String params);
}