class User {
  String token;
  String role;
  String userId;
  String name;

  User({this.token, this.role, this.userId});

  User.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    role = json['role'];
    userId = json['userId'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    data['role'] = this.role;
    data['userId'] = this.userId;
    data['name'] = this.name;
    return data;
  }
}