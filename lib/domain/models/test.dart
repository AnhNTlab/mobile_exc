class Data {
  String code;
  String name;
  String id;
  int quantity;
  int timeLimit;
  List<QuestionDtoList> questionDtoList;
  String idResult;

  Data(
      {this.code,
        this.name,
        this.id,
        this.quantity,
        this.timeLimit,
        this.questionDtoList,
        this.idResult});

  Data.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    name = json['name'];
    id = json['id'];
    quantity = json['quantity'];
    timeLimit = json['timeLimit'];
    if (json['questionDtoList'] != null) {
      questionDtoList = new List<QuestionDtoList>();
      json['questionDtoList'].forEach((v) {
        questionDtoList.add(new QuestionDtoList.fromJson(v));
      });
    }
    idResult = json['idResult'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['name'] = this.name;
    data['id'] = this.id;
    data['quantity'] = this.quantity;
    data['timeLimit'] = this.timeLimit;
    if (this.questionDtoList != null) {
      data['questionDtoList'] =
          this.questionDtoList.map((v) => v.toJson()).toList();
    }
    data['idResult'] = this.idResult;
    return data;
  }
}

class QuestionDtoList {
  String id;
  String name;
  List<AnswerDtos> answerDtos;

  QuestionDtoList(
      {this.id,
        this.name,
        this.answerDtos});

  QuestionDtoList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    if (json['answerDtos'] != null) {
      answerDtos = new List<AnswerDtos>();
      json['answerDtos'].forEach((v) {
        answerDtos.add(new AnswerDtos.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    if (this.answerDtos != null) {
      data['answerDtos'] = this.answerDtos.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AnswerDtos {
  String id;
  String name;

  AnswerDtos(
      {this.id, this.name});

  AnswerDtos.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}