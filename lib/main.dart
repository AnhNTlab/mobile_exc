import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mobile_exercise/app.dart';

import 'locator.dart';

void main() async {
  setupLocator();
  runZonedGuarded(() {
    runApp(
      MyApp(),
    );
  }, (error, stackTrace) {});
}
